package com.example.day2_priactice;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;


import com.example.day2_priactice.activitys.SecondActivvity;
import com.example.day2_priactice.adapter.XrecyclerAdapter;
import com.example.day2_priactice.bean.BeautyBean;
import com.example.day2_priactice.bean.MessageEvent;
import com.example.day2_priactice.contract.BeautyContract;
import com.example.day2_priactice.presenter.BeautyPresenter;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.youth.banner.loader.ImageLoader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BeautyContract.View {


    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.x_rv_view)
    XRecyclerView xRvView;
    private BeautyPresenter beautyPresenter;

    private List<BeautyBean.ResultsBean> myList = new ArrayList<>();
    private String path = "https://gank.io/api/data/%E7%A6%8F%E5%88%A9/10/";
    private int page = 2;
    private XrecyclerAdapter xrecyclerAdaapter;
    private String name;
    private int mposition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //注册

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        initOperate();
    }

    private void initOperate() {

        LinearLayoutManager layoutManager =  new  LinearLayoutManager(MainActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        xRvView.setLayoutManager(layoutManager);
        xRvView.setPullRefreshEnabled(true);
      //  xRvView.setLoadingMoreEnabled(true);
        xRvView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        xRvView.setLoadingMoreProgressStyle(ProgressStyle.Pacman);
        //获取presenter引用
        beautyPresenter = new BeautyPresenter(this);
        beautyPresenter.requestData(path + page);

        xRvView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                    page++;
                    if(page>10){
                        page--;
                    }
                   beautyPresenter.requestData(path+page);
                   Log.i(TAG, "onLoadMore: 请求的网址"+path+page);
                   new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            xRvView.refreshComplete();
                        }
                    }, 1000);
            }
            @Override
            public void onLoadMore() {
                  page--;
                  if(page<1){
                      page++;
                  }
                  beautyPresenter.requestData(path+page);
                Log.i(TAG, "onLoadMore: 请求的网址"+path+page);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        xRvView.refreshComplete();
                    }
                }, 1000);
            }
        });


    }


    @Override
    public void onSuccess(List<BeautyBean.ResultsBean> resultsBeanList) {
        Log.i(TAG, "onSuccess: 本页请求的数据长度"+resultsBeanList.size());
        if(resultsBeanList.size()>0){
             myList.addAll(resultsBeanList);
            xrecyclerAdaapter = new XrecyclerAdapter(this, myList);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!xrecyclerAdaapter.hasObservers()){
                        xRvView.setAdapter(xrecyclerAdaapter);
                    }else {
                        xrecyclerAdaapter.notifyDataSetChanged();
                    }
                }
            });
            xrecyclerAdaapter.setMitemClickListener(new XrecyclerAdapter.setOnItemClickListener() {
                @Override
                public void setOnItemClick(View view, int position) {
                    startActivity(new Intent(MainActivity.this,SecondActivvity.class));
                    mposition = position;
                    xrecyclerAdaapter.notifyDataSetChanged();
                }

            });

        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onEvent(MessageEvent messageEvent){
         name = messageEvent.getMessage();
        Log.i(TAG, "getEvent: 接收到的值"+name);
        xrecyclerAdaapter.getData(name,mposition);
        xrecyclerAdaapter.notifyDataSetChanged();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onError() {

    }
}
