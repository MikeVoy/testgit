package com.example.day2_priactice.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.day2_priactice.R;
import com.example.day2_priactice.bean.BeautyBean;
import com.facebook.drawee.view.SimpleDraweeView;
import com.stx.xhb.xbanner.XBanner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class XrecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = XrecyclerAdapter.class.getSimpleName();
    private Context context;
    private List<BeautyBean.ResultsBean> databeanList;


    public static final int TYPE1 = 1; //三张图
    public static final int TYPE2 = 2; //banner 轮播
    public static final int TYPE3 = 3; // 左图 右字
    public static final int TYPE4 = 4; // 左右图 中间字
    private int page = 4;
    public XrecyclerAdapter(Context context, List<BeautyBean.ResultsBean> myList) {
        this.context = context;
        this.databeanList = myList;
    }

  /*  public void onRefresh(List<BeautyBean.ResultsBean> data) {
        //Log.i(TAG, "onLoadMore: 请求的网址"+path+page);
        databeanList.addAll(databeanList);
        notifyDataSetChanged();

    }
    public void onLoadMore(List<BeautyBean.ResultsBean> data) {
        databeanList.addAll(data);
        notifyDataSetChanged();
        //   beautyPresenter.requestData(path+page);
      //  Log.i(TAG, "onLoadMore: 请求的网址"+path+page);
    }*/
    @Override
    public int getItemViewType(int position) {

        int i = 1;
        if (position == 0) {
           i=TYPE1;
        } else if (position == 1) {
            i = TYPE2;
        } else if (position%2==0) {
            i =TYPE3;
        } else if(position%2 == 1)  {
            i= TYPE4;
        }
       return i;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE1) {
            View view = LayoutInflater.from(context).inflate(R.layout.xrv_item_01, null, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        } else if (viewType == TYPE2) {
            View view = LayoutInflater.from(context).inflate(R.layout.xrv_item_02, null, false);
            ViewHolder02 viewHolder02 = new ViewHolder02(view);
            return viewHolder02;
        } else if (viewType == TYPE3) {
            View view = LayoutInflater.from(context).inflate(R.layout.xrv_item_03, null, false);
            ViewHolder03 viewHolder03 = new ViewHolder03(view);
            return viewHolder03;
        } else if (viewType == TYPE4) {
            View view = LayoutInflater.from(context).inflate(R.layout.xrv_item_04, null, false);
            ViewHolder04 viewHolder04 = new ViewHolder04(view);
            return viewHolder04;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        //获取数据
        databeanList.get(pos).setType(s);//进行 值的设置
        BeautyBean.ResultsBean resultsBean = databeanList.get(position);
        String url = resultsBean.getUrl();
        final List<String> images = new ArrayList<>();
        images.clear();
        for (int i = 0; i < 5; i++) {
            images.add(url);
        }
        //判断
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mitemClickListener.setOnItemClick(v,position);
                }
            });
            ((ViewHolder) holder).sdvImage01.setImageURI(url);
            ((ViewHolder) holder).sdvImage02.setImageURI(url);
            ((ViewHolder) holder).sdvImage03.setImageURI(url);
        } else if (holder instanceof ViewHolder02) {

            ((ViewHolder02) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mitemClickListener.setOnItemClick(v,position);
                }
            });
            if(((ViewHolder02) holder).mbVp!=null){
                ((ViewHolder02) holder).mbVp.removeAllViews();
            }
           ((ViewHolder02) holder).mbVp.setData(images,null);
            ((ViewHolder02) holder).mbVp.setmAdapter(new XBanner.XBannerAdapter() {
                @Override
                public void loadBanner(XBanner banner, View view, int position) {
                    Glide.with(context).load(images.get(position)).into((ImageView) view);
                }
            });

        } else if (holder instanceof ViewHolder03) {
            ((ViewHolder03) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mitemClickListener.setOnItemClick(v,position);
                }
            });

            ((ViewHolder03) holder).item03Sdv.setImageURI(url);
            Log.i(TAG, "onBindViewHolder: 得到了"+databeanList.get(position).getType());
            ((ViewHolder03) holder).item03TvContent.setText(databeanList.get(position).getType());
        } else if (holder instanceof ViewHolder04) {
            ((ViewHolder04) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mitemClickListener.setOnItemClick(v,position);
                }
            });
            ((ViewHolder04) holder).item04SdvLeft.setImageURI(url);
            ((ViewHolder04) holder).item04SdvRight.setImageURI(url);
            ((ViewHolder04) holder).item04TvContent.setText(resultsBean.getType());
        }

    }
    private String s;
    private int pos;
    public void getData(String s,int pos){
        this.s = s;
        this.pos = pos;
    }


    @Override
    public int getItemCount() {
        return databeanList == null ? 0 : databeanList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private View view;
        @BindView(R.id.sdv_image01)
        SimpleDraweeView sdvImage01;
        @BindView(R.id.sdv_image02)
        SimpleDraweeView sdvImage02;
        @BindView(R.id.sdv_image03)
        SimpleDraweeView sdvImage03;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }


    }


    class ViewHolder03 extends RecyclerView.ViewHolder {

        private View view;

        @BindView(R.id.item_03_sdv)
        SimpleDraweeView item03Sdv;
        @BindView(R.id.item_03_tv_content)
        TextView item03TvContent;

        ViewHolder03(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    class ViewHolder04 extends RecyclerView.ViewHolder {

        private View view;

        @BindView(R.id.item_04_sdv_left)
        SimpleDraweeView item04SdvLeft;
        @BindView(R.id.item_04_tv_content)
        TextView item04TvContent;
        @BindView(R.id.item_04_sdv_right)
        SimpleDraweeView item04SdvRight;

        ViewHolder04(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    class ViewHolder02 extends RecyclerView.ViewHolder {

        private View view;
        @BindView(R.id.mb_vp)
        XBanner mbVp;
        ViewHolder02(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }



    //定义内部接口
    public interface setOnItemClickListener{
         void setOnItemClick(View view,int position);
    }
    //创建私有变量,以及其set方法
    private setOnItemClickListener mitemClickListener;

    public void setMitemClickListener(setOnItemClickListener mitemClickListener) {
        this.mitemClickListener = mitemClickListener;
    }
}
