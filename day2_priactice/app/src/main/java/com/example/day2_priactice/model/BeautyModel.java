package com.example.day2_priactice.model;

import com.example.day2_priactice.bean.BeautyBean;
import com.example.day2_priactice.contract.BeautyContract;
import com.example.day2_priactice.utils.HttpUtils;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class BeautyModel implements BeautyContract.Model {


    @Override
    public void requestBeautyData(String path, final onDataListener dataListener) {

        HttpUtils.DOGET(path, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                   if(response.isSuccessful()){
                       String string = response.body().string();
                       Gson gson = new Gson();
                       BeautyBean beautyBean = gson.fromJson(string, BeautyBean.class);
                       List<BeautyBean.ResultsBean> results = beautyBean.getResults();

                       dataListener.onSuccess(results);
                   }
            }
        });
    }
}
