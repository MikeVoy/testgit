package com.example.day2_priactice.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.example.day2_priactice.R;
import com.example.day2_priactice.bean.MessageEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecondActivvity extends AppCompatActivity {

    @BindView(R.id.second_btn_change)
    Button secondBtnChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_activvity);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.second_btn_change)
    public void onViewClicked() {
                EventBus.getDefault().postSticky(new MessageEvent("王小二,就是你了!"));
        finish();
    }
}
