package com.example.day2_priactice.bean;

public class MessageEvent {

    private String message;

    public String getMessage() {
        return message;
    }

    public MessageEvent(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
