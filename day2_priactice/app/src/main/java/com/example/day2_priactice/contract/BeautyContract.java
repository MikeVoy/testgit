package com.example.day2_priactice.contract;

import com.example.day2_priactice.bean.BeautyBean;

import java.util.List;

public interface BeautyContract {
    interface Model {

        interface onDataListener{

            void onSuccess(List<BeautyBean.ResultsBean>resultsBeanList);
            void onError();
        }
        void requestBeautyData(String path,onDataListener dataListener);
    }

    interface View {

        void onSuccess(List<BeautyBean.ResultsBean>resultsBeanList);
        void onError();
    }

    interface Presenter {
        void requestData(String path);
    }
}
