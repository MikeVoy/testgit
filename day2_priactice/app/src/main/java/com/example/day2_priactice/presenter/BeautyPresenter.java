package com.example.day2_priactice.presenter;


import com.example.day2_priactice.bean.BeautyBean;
import com.example.day2_priactice.contract.BeautyContract;
import com.example.day2_priactice.model.BeautyModel;

import java.util.List;

public class BeautyPresenter implements BeautyContract.Presenter {

    private BeautyContract.View view;
    private BeautyModel beautyModel;

    public BeautyPresenter(BeautyContract.View view) {
        this.view = view;
        this.beautyModel = new BeautyModel();
    }

    @Override
    public void requestData(String path) {
            beautyModel.requestBeautyData(path, new BeautyContract.Model.onDataListener() {
                @Override
                public void onSuccess(List<BeautyBean.ResultsBean> resultsBeanList) {
                       view.onSuccess(resultsBeanList);
                }

                @Override
                public void onError() {

                }
            });
    }
}
