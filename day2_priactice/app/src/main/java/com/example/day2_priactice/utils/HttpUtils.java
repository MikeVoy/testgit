package com.example.day2_priactice.utils;

import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class HttpUtils {

    //懒汉模式
    private static OkHttpClient client = null;
    private HttpUtils() {
    }

    //提供获取client的实例
    public static OkHttpClient getClient(){

        if(client==null){

            synchronized (HttpUtils.class){
                if(client==null){
                    client = new OkHttpClient();
                }
            }

        }
        return client;
    }

    //开始进行网络资源请求
    //1 ; 使用异步GET请求
    public static  void DOGET(String path,Callback callback){
         //新建辅助类
        Request request = new Request.Builder()
                .url(path)
                .build();
        Call call = getClient().newCall(request);
        call.enqueue(callback);
    }



    //2 : post异步请求,一般使用String类型的键值对上传值

    public static  void DOPost(String path, Map<String,String>map,Callback callback){

        //使用post的花花
        //1 先创建表单请求体
        FormBody.Builder builder = new FormBody.Builder();
          for (String key:map.keySet()){
              builder.add(key,map.get(key));
          }
        FormBody formBody = builder.build();

          //创建辅助类
        Request request = new Request.Builder()
                .post(formBody)
                .build();
        Call call = getClient().newCall(request);
        call.enqueue(callback);

    }


}
